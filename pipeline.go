package pipeline

import (
	"errors"
	"log"
)

type Pipeline struct {
	Stages       []Stage
	Provider     Provider
	Consumer     Consumer
	FailConsumer FailConsumer
	doneChan     chan struct{}
	doneFailChan chan struct{}
}

// New создает новый конвейер
func New() *Pipeline {
	return &Pipeline{
		Stages:       []Stage{},
		doneChan:     make(chan struct{}),
		doneFailChan: make(chan struct{}),
	}
}

// AddStage добавляет очедедной этап в цепочку обработчиков
func (p *Pipeline) AddStage(id string, stage StageInterface, buffSize int, cntThread int) {
	newStage := Stage{
		id:        id,
		instance:  stage,
		buffSize:  buffSize,
		cntThread: cntThread,
	}
	p.Stages = append(p.Stages, newStage)
}

// SetProvider устанавливает источник сообщений
func (p *Pipeline) SetProvider(provider ProviderInterface) {
	p.Provider = Provider{
		instance: provider,
	}
}

// SetConsumer устанавливает потребителя обработанных сообщений
func (p *Pipeline) SetConsumer(consumer ConsumerInterface) {
	p.Consumer = Consumer{
		instance: consumer,
	}
}

// SetFailConsumer добавляет потребителя обрабатывающего ошибки
func (p *Pipeline) SetFailConsumer(consumer FailConsumerInterface) {
	p.FailConsumer = FailConsumer{
		instance: consumer,
	}
}

// Run запускает конвейер
func (p *Pipeline) Run() (err error) {
	if p.Provider.instance == nil {
		return errors.New("Не определен источник")
	}

	if p.Consumer.instance == nil {
		return errors.New("Не определен потребитель")
	}

	if p.FailConsumer.instance == nil {
		return errors.New("Не определен потребитель ошибок")
	}

	if len(p.Stages) == 0 {
		return errors.New("Не определены шаги обработки")
	}

	// Стартуем источник сообщений
	err = p.startProvider()
	if err != nil {
		log.Println("Не удалось стартовать источник. Error: ", err)
		return err
	}

	outChan := &p.Provider.outChan

	// Стартуем этапы обработки
	for _, stage := range p.Stages {
		outChan = p.startStage(stage, outChan)
	}

	// Стартуем потребителя
	p.startConsumer(outChan)

	// Стартуем потребителя
	p.startFailConsumer()

	return
}

// Stop останавливает конвейер
func (p *Pipeline) Stop() (err error) {
	return p.Provider.instance.Stop()
}

// Wait ждем завершения конвейеера
func (p *Pipeline) Wait() {
	<-p.doneChan

	if p.FailConsumer.instance != nil {
		// Все, что могло обработаться - обработалось. Значит ошибок больше не будет
		close(p.FailConsumer.inChan)

		// Ждем обработки ошибок
		<-p.doneFailChan
	}
}

// StartProvider Стартует источник сообщений
func (p *Pipeline) startProvider() (err error) {
	p.Provider.outChan = make(chan interface{}, 1)
	err = p.Provider.instance.Run(&p.Provider.outChan)

	return
}

// StartConsumer Стартует потребителя обработанных сообщений
func (p *Pipeline) startConsumer(inChan *chan interface{}) {
	p.Consumer.inChan = inChan

	go func() {
		for msg := range *p.Consumer.inChan {
			err := p.Consumer.instance.Exec(msg)
			if err != nil {
				log.Println("Error:", err)
			}
		}

		log.Println("Close Consumer")

		close(p.doneChan)
	}()
}

// startFailConsumer Стартует потребителя обработанных сообщений
func (p *Pipeline) startFailConsumer() {
	p.FailConsumer.inChan = make(chan StageError, 100)

	go func() {
		for msg := range p.FailConsumer.inChan {
			p.FailConsumer.instance.Exec(msg)
		}

		log.Println("Close FailConsumer")
		// close(p.FailConsumer.inChan)
		close(p.doneFailChan)
	}()
}

func (p *Pipeline) startStage(stg Stage, inChan *chan interface{}) (outChan *chan interface{}) {
	ch := make(chan interface{}, stg.buffSize)

	// TODO: Чтобы запустить несколько потоков, нужно для каждого потока создать свой outChan,
	// закрывать их и т.д. => `mergeChannels` в отдельной горутине
	// for i := 0; i < stg.cntThread; i++ {
	go func() {
		for msg := range *inChan {
			resp, err := stg.instance.Exec(msg)

			if err != nil {
				log.Println(">>>>>", err)

				if p.FailConsumer.instance != nil {
					p.FailConsumer.inChan <- StageError{
						stgID:    stg.id,
						request:  msg,
						response: resp,
						err:      err,
					}
				}
			} else {
				if resp != nil {
					ch <- resp
				}
			}
		}

		log.Println("Stop ", stg.id)
		close(ch)
	}()
	// }

	return &ch
}
