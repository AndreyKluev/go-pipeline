package pipeline

// Request
type Request struct {
	Data interface{}
}

// Response
type Response struct {
	Error error
	Data  interface{}
}

// Stage
type Stage struct {
	id        string
	instance  StageInterface
	buffSize  int
	cntThread int
	inChan    *chan interface{}
	outChan   chan interface{}
}

// Provider
type Provider struct {
	instance ProviderInterface
	outChan  chan interface{}
}

// Consumer
type Consumer struct {
	instance ConsumerInterface
	inChan   *chan interface{}
}

// FailConsumer
type FailConsumer struct {
	instance FailConsumerInterface
	inChan   chan StageError
}

type StageError struct {
	stgID    string
	request  interface{}
	response interface{}
	err      error
}

// StageInterface
type StageInterface interface {
	Exec(interface{}) (interface{}, error)
}

// ProviderInterface
type ProviderInterface interface {
	Run(*chan interface{}) error
	Stop() error
}

// ConsumerInterface
type ConsumerInterface interface {
	Exec(interface{}) error
}

// FailConsumerInterface
type FailConsumerInterface interface {
	Exec(StageError)
}
