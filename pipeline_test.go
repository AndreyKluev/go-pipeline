package pipeline

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSetProvider(t *testing.T) {
	provider := NewTestProvider(false)

	p := New()
	p.SetProvider(provider)

	assert.Equal(t, Provider{instance: provider}, p.Provider, "")
}

func TestSetConsumer(t *testing.T) {
	consumer := NewTestConsumer()

	p := New()
	p.SetConsumer(consumer)

	assert.Equal(t, Consumer{instance: consumer}, p.Consumer, "")
}

func TestAddStage(t *testing.T) {
	stage1 := NewTestStage()
	stage2 := NewTestStage()
	stage3 := NewTestStage()

	p := New()
	p.AddStage("st1", stage1, 1, 1)

	assert.Equal(t, Stage{id: "st1", instance: stage1, buffSize: 1, cntThread: 1}, p.Stages[0], "1-ый шаг")
	assert.Equal(t, len(p.Stages), 1, "Кол-во шагов")

	p.AddStage("st2", stage2, 2, 2)
	p.AddStage("st3", stage3, 3, 3)

	assert.Equal(t, Stage{id: "st2", instance: stage2, buffSize: 2, cntThread: 2}, p.Stages[1], "2-ой шаг")
	assert.Equal(t, Stage{id: "st3", instance: stage3, buffSize: 3, cntThread: 3}, p.Stages[2], "3-ий шаг")
	assert.Equal(t, len(p.Stages), 3, "Кол-во шагов")
}

func TestStopWithoutRun(t *testing.T) {
	var err error
	provider := NewTestProvider(false)

	p := New()
	p.SetProvider(provider)
	err = p.Stop()

	assert.Nil(t, err)
}

func TestFailStopWithoutRun(t *testing.T) {
	var err error
	provider := NewTestProvider(true)

	p := New()
	p.SetProvider(provider)
	err = p.Stop()

	assert.Equal(t, err, errors.New("Fail TestProvider Stop"), "Провайдер остановлен с ошибкой")
}

func TestStartProvider(t *testing.T) {
	var err error
	provider := NewTestProvider(false)

	p := New()
	p.SetProvider(provider)
	err = p.startProvider()

	assert.Nil(t, err)
}

func TestFailStartProvider(t *testing.T) {
	var err error
	provider := NewTestProvider(true)

	p := New()
	p.SetProvider(provider)
	err = p.startProvider()

	assert.Equal(t, err, errors.New("Fail TestProvider Run"), "Провайдер запущен с ошибкой")
}

func TestRunPipelineWithoutProvider(t *testing.T) {
	var err error

	p := New()
	err = p.Run()

	assert.Equal(t, err, errors.New("Не определен источник"), "Провайдер не запущен")
}

func TestRunPipelineWithoutConsumer(t *testing.T) {
	var err error
	provider := NewTestProvider(false)

	p := New()
	p.SetProvider(provider)
	err = p.Run()

	assert.Equal(t, err, errors.New("Не определен потребитель"), "Провайдер не запущен")
}

func TestRunPipelineWithoutFailConsumer(t *testing.T) {
	var err error
	provider := NewTestProvider(false)
	consumer := NewTestConsumer()

	p := New()
	p.SetProvider(provider)
	p.SetConsumer(consumer)
	err = p.Run()

	assert.Equal(t, err, errors.New("Не определен потребитель ошибок"), "Провайдер не запущен")
}

func TestRunPipelineWithoutStages(t *testing.T) {
	var err error
	provider := NewTestProvider(false)
	consumer := NewTestConsumer()

	p := New()
	p.SetProvider(provider)
	p.SetConsumer(consumer)
	p.SetFailConsumer(NewTestFailConsumer())
	err = p.Run()

	assert.Equal(t, err, errors.New("Не определены шаги обработки"), "Провайдер не запущен")
}

func TestRunPipelineWithFailProvider(t *testing.T) {
	var err error
	provider := NewTestProvider(true)
	consumer := NewTestConsumer()

	p := New()
	p.SetProvider(provider)
	p.SetConsumer(consumer)
	p.SetFailConsumer(NewTestFailConsumer())
	p.AddStage("st1", NewTestStage(), 1, 1)
	err = p.Run()

	assert.Equal(t, err, errors.New("Fail TestProvider Run"), "Конвейер не запущен")
}

func TestStartConsumer(t *testing.T) {
	consumer := NewTestConsumer()

	p := New()
	p.SetConsumer(consumer)

	inChan := make(chan interface{}, 10)
	inChan <- 1
	inChan <- 2
	inChan <- 3
	inChan <- -1
	close(inChan)

	p.startConsumer(&inChan)

	p.Wait()

	assert.Equal(t, p.Consumer.instance, &TestConsumer{Results: []int{1, 2, 3}})
}

func TestAllInOne(t *testing.T) {
	var err error
	provider := NewTestProvider(false)
	consumer := NewTestConsumer()

	p := New()
	p.SetProvider(provider)
	p.SetConsumer(consumer)
	p.SetFailConsumer(NewTestFailConsumer())
	p.AddStage("st1", NewTestStage(), 1, 1)
	err = p.Run()

	provider.Send(1)
	provider.Send(-1)
	provider.Send(2)
	provider.Send(-2)
	provider.Send(3)
	provider.Send(-3)
	provider.Send(4)

	close(p.Provider.outChan)

	// Ожидаем завершения
	p.Wait()

	assert.Nil(t, err)
	assert.Equal(t, p.Consumer.instance, &TestConsumer{Results: []int{1, 2, 3, 4}})
}

//

type TestProvider struct {
	outChan  *chan interface{}
	isFailed bool
}

func NewTestProvider(isFailed bool) *TestProvider {
	return &TestProvider{
		isFailed: isFailed,
	}
}

func (tp *TestProvider) Run(outChan *chan interface{}) (err error) {
	tp.outChan = outChan

	if tp.isFailed == true {
		return errors.New("Fail TestProvider Run")
	}

	return
}

func (tp *TestProvider) Stop() (err error) {
	if tp.isFailed == true {
		return errors.New("Fail TestProvider Stop")
	}

	return
}

func (tp *TestProvider) Send(x int) {
	*tp.outChan <- x
}

type TestConsumer struct {
	isFailed bool
	Results  []int
}

func NewTestConsumer() *TestConsumer {
	return &TestConsumer{
		Results: make([]int, 0),
	}
}

func (tc *TestConsumer) Exec(request interface{}) (err error) {
	if tc.isFailed == true {
		return errors.New("Fail TestConsumer Run")
	}

	if request.(int) == -1 {
		return errors.New("Bad Request")
	}

	tc.Results = append(tc.Results, request.(int))

	return
}

type TestStage struct {
}

func NewTestStage() *TestStage {
	return &TestStage{}
}

func (t *TestStage) Exec(request interface{}) (response interface{}, err error) {
	if request.(int) < 0 {
		return nil, fmt.Errorf("Bad Request: %d", request.(int))
	}

	return request.(int), nil
}

type TestFailConsumer struct {
}

func NewTestFailConsumer() *TestFailConsumer {
	return &TestFailConsumer{}
}

func (t *TestFailConsumer) Exec(err StageError) {
	// log.Println("Catch error: ", err)
}
